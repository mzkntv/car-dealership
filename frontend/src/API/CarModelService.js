import { faker } from '@faker-js/faker';
import axios from 'axios';

export default class CarModelService {

    static async getAllFromServer()
    {
        try {
            const response = await axios.get('https://myfakeapi.com/api/cars');
            return response.data.cars;
        }
        catch(error) {
            console.log(error);
        }
    }

    static async getCarDetailsByVIN(vin)
    {
        const apiKey = 'ZrQEPSkKZGlta2FtdW5vdkBnbWFpbC5jb20=';
        try {
            const response = await axios.get(`https://auto.dev/api/vin/${vin}`, {
                params: {
                    apikey: apiKey
                }
            });

            return response.data;
        }
        catch(error) {
            console.log(error);
        }
    }

    static async getAllNCat()
    {
        try {
            const response = await axios.get('https://vpic.nhtsa.dot.gov/api/vehicles/GetAllMakes?format=json');
            return response.data;
        }
        catch(error) {
            console.log(error);
        }
    }
}