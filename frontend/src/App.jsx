import React from 'react';
import { Routes, Route } from 'react-router-dom';
import CarDetails from './components/Cars/CarDetails';
import Layout from './components/Layout';
import CarList from './pages/CarList';
import Models from './pages/Models';

const App = () => {
  return (
    <Routes>
      <Route path="/" element={<Layout />} >
        <Route index element={<div> Main Page </div>} />
        <Route path="/cars" element={<CarList />} />
        <Route path="/cars/:vin" element={<CarDetails />} />
        <Route path="/models" element={<Models />} />
        <Route path="*" element={<div> Empty Page </div>} />
      </Route>
    </Routes>
  )
}

export default App;