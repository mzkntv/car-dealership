import { ExpandMore } from '@mui/icons-material';
import { 
    Accordion, 
    AccordionDetails, 
    AccordionSummary, 
    Grid, 
    Paper, 
    Typography 
} from '@mui/material';
import React from 'react';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { carDetailLoad } from '../../redux/cars/actions';
import PropertyGrid from '../ProperyGrid/PropertyGrid';

const CarDetails = () => {
    const params = useParams();
    const dispatch = useDispatch();
    const details = useSelector(state => {
        const { carsReducer } = state;
        return carsReducer.carDetails;
    });

    console.log(details);

    useEffect(() => {
        const { vin } = params;
        dispatch(carDetailLoad(vin));
    }, []);

    return (
        <Grid container spacing={2}>
            <Grid item xs={12}>
                <PropertyGrid
                    title="Categories"
                    propertyNames={[{
                        dataIndex: 'crossover',
                        text: 'Crossover'
                    },{
                        dataIndex: 'epaClass',
                        text: 'EpaClass'
                    },{
                        dataIndex: 'market',
                        text: 'Market'  
                    },{
                        dataIndex: 'primaryBodyType',
                        text: 'PrimaryBodyType'
                    },{
                        dataIndex: 'vehicleSize',
                        text: 'VehicleSize'
                    },{
                        dataIndex: 'vehicleStyle',
                        text: 'VehicleStyle'
                    },{
                        dataIndex: 'vehicleType',
                        text: 'VehicleType'
                    }]}
                    propertyData={details ? details.categories : {}}
                />
            </Grid>
            {(details && details.price) ? <Grid item xs={12}>
                <PropertyGrid 
                    title="Price"
                    propertyNames={[{
                        dataIndex: 'baseInvoice',
                        text: 'BaseInvoice'
                    },{
                        dataIndex: 'baseMsrp',
                        text: 'BaseMsrp'
                    },{
                        dataIndex: 'deliveryCharges',
                        text: 'DeliveryCharges'  
                    },{
                        dataIndex: 'estimateTmv',
                        text: 'EstimateTmv'
                    },{
                        dataIndex: 'tmvRecommendedRating',
                        text: 'TmvRecommendeRating'
                    },{
                        dataIndex: 'usedPrivateParty',
                        text: 'UsedPrivateParty'
                    },{
                        dataIndex: 'usedTmvRetail',
                        text: 'UsedTmvRetail'
                    },{
                        dataIndex: 'usedTradeIn',
                        text: 'UsedTradeIn'
                    }]}
                    propertyData={details ? details.price : {}}
                />
            </Grid> : ''}
            <Grid item xs={12}>
                <PropertyGrid
                    title="Engine"
                    propertyNames={[{
                        dataIndex: 'availability',
                        text: 'Availability'
                    },{
                        dataIndex: 'code',
                        text: 'Code'
                    },{
                        dataIndex: 'compressionRatio',
                        text: 'CompressionRatio'
                    },{
                        dataIndex: 'compressorType',
                        text: 'CompressorType'
                    },{
                        dataIndex: 'configuration',
                        text: 'Configuration'
                    },{
                        dataIndex: 'cylinder',
                        text: 'Cylinder'
                    },{
                        dataIndex: 'displacement',
                        text: 'Displacement'
                    },{
                        dataIndex: 'equipmentType',
                        text: 'EquipmentType'
                    },{
                        dataIndex: 'fuelType',
                        text: 'FuelType'
                    },{
                        dataIndex: 'horsepower',
                        text: 'HorsePower'
                    },{
                        dataIndex: 'id',
                        text: 'Id'
                    },{
                        dataIndex: 'name',
                        text: 'Name'
                    },{
                        dataIndex: 'size',
                        text: 'Size'
                    },{
                        dataIndex: 'totalValves',
                        text: 'TotalValves'
                    },{
                        dataIndex: 'type',
                        text: 'Type'
                    }]}
                    propertyData={details ? details.engine : {}}
                />
            </Grid>
            <Grid item xs={12}>
                <PropertyGrid
                    title="Transmission"
                    propertyNames={[{
                        dataIndex: 'availability',
                        text: 'Availability'
                    },{
                        dataIndex: 'equipmentType',
                        text: 'EquipmentType'
                    },{
                        dataIndex: 'id',
                        text: 'Id'
                    },{
                        dataIndex: 'name',
                        text: 'Name'
                    },{
                        dataIndex: 'numberOfSpeeds',
                        text: 'NumberOfSpeeds'  
                    },{
                        dataIndex: 'transmissionType',
                        text: 'TransmissionType'
                    }]}
                    propertyData={details ? details.transmission : {}}
                />
            </Grid>
            <Grid item xs={12}>
                <PropertyGrid
                    title="Make"
                    propertyNames={[{
                        dataIndex: 'id',
                        text: 'Id'
                    },{
                        dataIndex: 'name',
                        text: 'Name'
                    },{
                        dataIndex: 'niceName',
                        text: 'NiceName'  
                    }]}
                    propertyData={details ? details.make : {}}
                />
            </Grid>
            <Grid item xs={12}>
                <PropertyGrid
                    title="Model"
                    propertyNames={[{
                        dataIndex: 'id',
                        text: 'Id'
                    },{
                        dataIndex: 'name',
                        text: 'Name'
                    },{
                        dataIndex: 'niceName',
                        text: 'NiceName'  
                    }]}
                    propertyData={details ? details.model : {}}
                />
            </Grid>
        </Grid>
    )
}

export default CarDetails;