import { Card, CardMedia, CardContent, Typography, Breadcrumbs, CardActionArea, CircularProgress } from '@mui/material';
import React from 'react';
import carImage from '@images/rio.png';
import { useNavigate, useLocation } from 'react-router-dom';

const CarItem = (props) => {
    const navigate = useNavigate();
    const location = useLocation();

    const handleClick = () => {
        navigate(`${location.pathname}/${props.car.car_vin}`);
    }

    return (
        <div>
            {props.car
                ?
                <Card>
                    <CardActionArea onClick={handleClick}>
                        <CardMedia
                            component="img"
                            image={carImage}
                        />
                    </CardActionArea>
                    <CardContent>
                        <Typography sx={{fontWeight: 'bold'}}>{props.car.car} {props.car.car_model} {props.car.car_model_year}</Typography>
                        <Typography></Typography>
                        <Breadcrumbs>
                            <Typography color="text.primary">VIN</Typography>
                            <Typography color="text.primary">{props.car.car_vin}</Typography>
                        </Breadcrumbs>
                        <Typography>Color: {props.car.car_color}</Typography>
                        <Typography sx={{fontWeight: 'bold'}}>Cost: {props.car.price}</Typography>
                    </CardContent>
                </Card>
                :
                <CircularProgress />
            }
        </div>
    )
}

export default CarItem;