import { Card, CardContent, CardMedia, Paper, Skeleton } from '@mui/material';
import React from 'react';

const CarSkeleton = () => {
    return (
        <Card>
            <CardMedia>
                <Skeleton variant='rectangular' height={200}/>
            </CardMedia>
            <CardContent>
                <Skeleton variant='text' height={30}/>
                <Skeleton variant='text' height={30}/>
                <Skeleton variant='text' height={30}/>
                <Skeleton variant='text' height={30}/>
            </CardContent>
        </Card>
        
    )
}

export default CarSkeleton;