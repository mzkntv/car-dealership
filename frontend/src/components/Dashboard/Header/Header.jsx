import { Paper } from '@mui/material';
import React from 'react';
import { useState, useEffect } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import HeaderButton from './HeaderButton';

const Header = () => {
  let navigate = useNavigate();
  let location = useLocation();

  return (
    <Paper sx={{borderRadius: '0px', marginBottom: '10px'}}>
      <HeaderButton onClick={() =>  { navigate("models") }} sx={{height: '70px'}}>Модели</HeaderButton>
      <HeaderButton onClick={() =>  { navigate("cars") }} sx={{height: '70px'}}>Выбор и покупка</HeaderButton>
      <HeaderButton onClick={() =>  { navigate("/") }} sx={{height: '70px'}}>Конфигуратор</HeaderButton>
      <HeaderButton sx={{height: '70px'}}>Авто в наличии</HeaderButton>
      <HeaderButton sx={{height: '70px'}}>Авто с пробегом</HeaderButton>
      <HeaderButton sx={{height: '70px'}}>Владельцам</HeaderButton>
    </Paper>
  );
}

export default Header;