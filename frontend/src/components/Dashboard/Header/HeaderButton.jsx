import { styled } from '@mui/material/styles';
import { Button } from '@mui/material';

const HeaderButton = styled(Button)({
    color: 'black',
    borderRadius: '0px',

    '&:hover': {
        color: 'white',
        backgroundColor: 'black'
    }
});

export default HeaderButton