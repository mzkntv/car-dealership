import { Grid, Typography } from '@mui/material';
import React from 'react';
import styles from './PropertyGrid.module.scss';

const PropertyGrid = ({propertyNames, propertyData, title}) => {
    return (
        propertyData ? <Grid className={styles.Body} container>
            {
                propertyNames.map((property, i) => {
                    return ((i === 0 && title) 
                    ? <Grid item container key={i} className={styles.Header}>
                        <div className={styles.Cell}>
                            <Typography sx={{fontWeight: 'bold'}}>
                                {title}
                            </Typography>
                        </div>
                    </Grid>
                    : propertyData[property.dataIndex] !== undefined ?
                        <Grid item container className={styles.Row} key={property.dataIndex}>
                            <Grid item xs={6}>
                                <div className={styles.Cell}>
                                    <Typography sx={{fontWeight: 'bold'}}>
                                        {property.text}
                                    </Typography>
                                </div>
                            </Grid>
                            <Grid item xs={6}>
                                <div className={styles.Cell}>
                                    <Typography>
                                        {propertyData[property.dataIndex].toString()}
                                    </Typography>
                                </div>
                            </Grid>
                        </Grid>
                        : ''
                )})
            }
        </Grid>
        : ''
    )
}

export default PropertyGrid;