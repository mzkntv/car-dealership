import { CircularProgress, Grid } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import CarModelService from '../API/CarModelService';
import CarItem from '../components/Cars/CarItem';
import CarSkeleton from '../components/Cars/CarSkeleton';
import { carsLoad } from '../redux/cars/actions';

const CarList = () => {
    const dispatch = useDispatch();
    const cars = useSelector(state => {
        const { carsReducer } = state;
        return carsReducer.cars;
    });

    useEffect(() => {
        dispatch(carsLoad());
    }, []);

    return (
        <Grid container spacing={2}>
        {
            cars.length
            ? cars.slice(0, 12).map(car =>
                <Grid item key={car.car_vin} xs={12} sm={6} md={4} lg={3}>
                    <CarItem car={car} />
                </Grid>
            )
            :
            Array(12).fill(0).map((car, index) => 
                <Grid item key={index} xs={12} sm={6} md={4} lg={3}>
                    <CarSkeleton />
                </Grid>    
            )
        }
        </Grid>
    )
}

export default CarList;