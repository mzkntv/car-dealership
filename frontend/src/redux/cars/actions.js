import CarModelService from '../../API/CarModelService';
import { CARS_LOAD, CAR_DETAILS_LOAD } from './actionTypes';

export function carsLoad() {
    return async dispatch => {
        const data = await CarModelService.getAllFromServer();

        dispatch({
            type: CARS_LOAD,
            cars: data
        });
    }
}

export function carDetailLoad(vin) {
    return async dispatch => {
        const data = await CarModelService.getCarDetailsByVIN(vin);

        dispatch({
            type: CAR_DETAILS_LOAD,
            details: data
        });
    }
}