import { CARS_LOAD, CAR_DETAILS_LOAD } from './actionTypes';

const initialState = {
    cars: [],
    carDetails: null
}

export default function carsReducer(state = initialState, action){
    switch (action.type) {
        case CARS_LOAD: {
            return {
                ...state,
                cars: action.cars
            }
        }

        case CAR_DETAILS_LOAD: {
            return {
                ...state,
                carDetails: action.details
            }
        }
        
        default: {
            return state;
        }
    }
}