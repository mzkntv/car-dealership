import { combineReducers } from "redux";
import carsReducer from "./cars/reducer";

export const rootReducer = combineReducers({
    carsReducer
});